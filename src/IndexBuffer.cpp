#include "IndexBuffer.h"

#include "glcall.h"


IndexBuffer::IndexBuffer(int unsigned const * data, int unsigned count)
    : m_Count(count)
{
    GlCall(glGenBuffers(1, &m_RendererId));
    GlCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_RendererId));
    GlCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(int unsigned), data, GL_STATIC_DRAW));
}


IndexBuffer::~IndexBuffer() {
    GlCall(glDeleteBuffers(1, &m_RendererId));
}


void IndexBuffer::Bind() const {
    GlCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_RendererId));
}


void IndexBuffer::Unbind() const {
    GlCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
}

int unsigned IndexBuffer::GetCount() const {
    return m_Count;
}
