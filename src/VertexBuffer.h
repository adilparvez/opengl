#pragma once


class VertexBuffer {
private:
    int unsigned m_RendererId;
public:
    VertexBuffer(void const * data, int unsigned size);
    ~VertexBuffer();
    void Bind() const;
    void Unbind() const;
};
