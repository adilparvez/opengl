#include <fstream>
#include <string>
#include <sstream>

#include <fmt/format.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "glcall.h"
#include "IndexBuffer.h"
#include "VertexBuffer.h"


int const WIDTH = 1280;
int const HEIGHT = 720;


struct ShaderProgramSource {
    std::string vertexShaderSource;
    std::string fragmentShaderSource;
};


static ShaderProgramSource ParseShader(std::string const & path) {
    std::ifstream stream(path);

    enum class ShaderType {
        NONE,
        VERTEX,
        FRAGMENT
    };

    std::stringstream vertexShader;
    std::stringstream fragmentShader;

    ShaderType type = ShaderType::NONE;
    std::string line;
    while (getline(stream, line)) {
        if (line.find("#shader") != std::string::npos) {
            if (line.find("vertex") != std::string::npos) {
                type = ShaderType::VERTEX;
            }
            if (line.find("fragment") != std::string::npos) {
                type = ShaderType::FRAGMENT;
            }
        } else {
            switch (type) {
                case ShaderType::VERTEX:
                    vertexShader << line << std::endl;
                    break;
                case ShaderType::FRAGMENT:
                    fragmentShader << line << std::endl;
                    break;
            }
        }
    }

    return {
        vertexShader.str(),
        fragmentShader.str()
    };
}


static int unsigned CompileShader(int unsigned type, std::string const & source) {
    GlCall(int unsigned shader = glCreateShader(type));
    char const * cSource = source.c_str();
    GlCall(glShaderSource(shader, 1, &cSource, nullptr));
    GlCall(glCompileShader(shader));

    int result;
    GlCall(glGetShaderiv(shader, GL_COMPILE_STATUS, &result));
    if (result == GL_FALSE) {
        int bufferSize;
        GlCall(glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &bufferSize));

        std::unique_ptr<char[]> infoLog = std::make_unique<char[]>(bufferSize);
        int length;
        GlCall(glGetShaderInfoLog(shader, bufferSize, &length, infoLog.get()));
        if (type == GL_VERTEX_SHADER) {
            fmt::print(stderr, "ERROR: VERTEX_SHADER: ");
        }
        if (type == GL_FRAGMENT_SHADER) {
            fmt::print(stderr, "ERROR: FRAGMENT_SHADER: ");
        }
        fmt::print(stderr, infoLog.get());

        GlCall(glDeleteShader(shader));

        return 0;
    }

    return shader;
}


static int unsigned CreateShader(std::string const & vertexShaderSource, std::string const & fragmentShaderSource) {
    GlCall(int unsigned program = glCreateProgram());
    int unsigned vertexShader = CompileShader(GL_VERTEX_SHADER, vertexShaderSource);
    int unsigned fragmentShader = CompileShader(GL_FRAGMENT_SHADER, fragmentShaderSource);

    GlCall(glAttachShader(program, vertexShader));
    GlCall(glAttachShader(program, fragmentShader));
    GlCall(glLinkProgram(program));
    GlCall(glValidateProgram(program));

    GlCall(glDeleteShader(vertexShader));
    GlCall(glDeleteShader(fragmentShader));

    return program;
}


int main(int argc, char * argv[]) {
    if (!glfwInit()) {
        fmt::print(stderr, "ERROR: failed to start GLFW\n");
        return -1;
    }
    fmt::print("INFO: GLFW: {}\n", glfwGetVersionString());

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    GLFWwindow * window = glfwCreateWindow(WIDTH, HEIGHT, "App", nullptr, nullptr);
    if (!window) {
        fmt::print(stderr, "ERROR: failed to create OpenGL 3.3+ context\n");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    fmt::print("INFO: GPU: {}\n", glGetString(GL_RENDERER));
    fmt::print("INFO: OpenGL: {}\n", glGetString(GL_VERSION));

    GLenum error = glewInit();
    if (error != GLEW_OK) {
        fmt::print(stderr, "ERROR: {}\n", glewGetErrorString(error));
    }
    fmt::print("INFO: GLEW: {}\n", glewGetString(GLEW_VERSION));

    {
        float positions[] = {
            -0.5f, -0.5f,
            0.5f, -0.5f,
            0.5f,  0.5f,
            -0.5f,  0.5f
        };

        int unsigned indices[] = {
            0, 1, 2,
            2, 3, 0
        };

        int unsigned vertexArray;
        GlCall(glGenVertexArrays(1, &vertexArray));
        GlCall(glBindVertexArray(vertexArray));

        VertexBuffer vertexBuffer(positions, 8 * sizeof(float));

        GlCall(glEnableVertexAttribArray(0));
        GlCall(glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0));

        IndexBuffer indexBuffer(indices, 6);

        ShaderProgramSource source = ParseShader("res/shaders/basic.shader");
        int unsigned program = CreateShader(source.vertexShaderSource, source.fragmentShaderSource);
        GlCall(glUseProgram(program));

        GlCall(int location = glGetUniformLocation(program, "u_Color"));

        GlCall(glBindVertexArray(0));
        GlCall(glUseProgram(0));
        GlCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
        GlCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));

        float r = 0.0f;
        float increment = 0.02f;

        while (!glfwWindowShouldClose(window)) {
            GlCall(glClear(GL_COLOR_BUFFER_BIT));

            GlCall(glUseProgram(program));
            GlCall(glUniform4f(location, r, 0.3f, 0.8f, 1.0f));

            GlCall(glBindVertexArray(vertexArray));
            indexBuffer.Bind();

            GlCall(glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr));

            if (r > 1.0f || r < 0.0f) {
                increment *= -1.0f;
            }

            r += increment;

            glfwSwapBuffers(window);
            glfwPollEvents();
        }

        GlCall(glDeleteProgram(program));
    }

    glfwTerminate();

    return 0;
}
