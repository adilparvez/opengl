#pragma once


class IndexBuffer {
private:
    int unsigned m_RendererId;
    int unsigned m_Count;
public:
    IndexBuffer(int unsigned const * data, int unsigned count);
    ~IndexBuffer();
    void Bind() const;
    void Unbind() const;
    int unsigned GetCount() const;
};
