#pragma once

#include <GL/glew.h>


#define GlCall(x)\
    GlClearErrors();\
    x;\
    GlLogCall(#x, __FILE__, __LINE__);


void GlClearErrors();
void GlLogCall(char const * function, char const * file, int line);
