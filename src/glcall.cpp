#include "glcall.h"

#include <fmt/format.h>
#include <GL/glew.h>


void GlClearErrors() {
    while (glGetError() != GL_NO_ERROR) {}
}


void GlLogCall(char const * function, char const * file, int line) {
    GLenum error;
    while ((error = glGetError()) != GL_NO_ERROR) {
        fmt::print("ERROR: OpenGL: {} {}:{} {} {}\n", function, file, line, error, gluErrorString(error));
    }
}
